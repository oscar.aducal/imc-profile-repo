$(document).ready(function($) {
    $(window).scroll(function() {

        if ($(window).scrollTop() > 100) {
            $('#navbar-main').addClass('fixed-top');
            $("main").css("margin-top", "70px");
        } else {
            $('#navbar-main').removeClass('fixed-top');
            $("main").css("margin-top", "0px");
        }
    });


    $('#gallery').carousel({
        interval: 5000
      })
      
      // Modify each slide to contain five columns of images
      $('#gallery.carousel .carousel-item').each(function(){
          var minPerSlide = 4;
          var next = $(this).next();
          if (!next.length) {
          next = $(this).siblings(':first');
          }
          next.children(':first-child').clone().appendTo($(this));
          
          for (var i=0;i<minPerSlide;i++) {
              next=next.next();
              if (!next.length) {
                  next = $(this).siblings(':first');
                }
              
              next.children(':first-child').clone().appendTo($(this));
            }
      });
      
      // Initialize carousel
      $( ".carousel-item:first-of-type" ).addClass( "active" );
      $( ".carousel-indicators:first-child" ).addClass( "active" );


      $('.service_link').click(function (e) {
        e.preventDefault();
        var content = $(this).html();
        $('.modal-body').html(content);
        $('.modal-body').children().addClass('card-img');
        $('.modal-body').children().removeClass('card-img-top-sm card-img-top-md card-img-top-lg');
        $('.modal-body').children().children('.card-text').removeClass('text-limitz');
        $('#modal').modal('show');
    });

    function updateBackgroundPosition() {
        var viewportWidth = $(window).width();
        var rightPosition = -90; // Initial position
        if (viewportWidth <= 991) {
            rightPosition = -70; // Adjusted position for smaller screens
        }
        if (viewportWidth <= 767) {
            rightPosition = -50; // Adjusted position for even smaller screens
        }
        $('.imc-divider::after').css('right', rightPosition + '%');
    }

    // Initial call to set background position based on initial viewport size
    updateBackgroundPosition();

    // Update background position on window resize
    $(window).resize(function() {
        updateBackgroundPosition();
    });


    $(document).ready(function() {
        // Check if the cookie is set
        if (document.cookie.indexOf("privacyAccepted=true") >= 0) {
            // Check if the myModalShown cookie is not set
            // if (document.cookie.indexOf("myModalShown=true") < 0) {
            //     // Show the myModal modal after a 3-second delay
            //     setTimeout(function() {
            //         $('#myModal').modal('show');
            //     }, 1000); // 3 seconds
            // }
        } else {
            // Show the data privacy modal if the privacyAccepted cookie is not set
            $('#data-privacy-popup').modal('show');
        }
    
        // Handle the "Accept" button click
        $('#accept-button').click(function() {
            // Set a cookie to remember the user's choice
            var expires = new Date();
            expires.setTime(expires.getTime() + 7 * 24 * 60 * 60 * 1000); // 7 day
            document.cookie = "privacyAccepted=true; expires=" + expires.toUTCString() + "; path=/";
    
            // Hide the data privacy modal
            $('#data-privacy-popup').modal('hide');
    
            // Show the myModal modal after a 3-second delay
            // setTimeout(function() {
            //     $('#myModal').modal('show');
            // }, 1000); // 3 seconds
        });


    
        // Handle the "I Reject" button click
        $('#reject-button').click(function() {
            // Set a cookie to remember the user's choice
            var expires = new Date();
            expires.setTime(expires.getTime() + 1 * 24 * 60 * 60 * 1000); // 1 day
            document.cookie = "privacyRejected=true; expires=" + expires.toUTCString() + "; path=/";
    
            // Hide the modal
            $('#data-privacy-popup').modal('hide');
        });
    
        // Handle the "Don't show it again" button click
        $('#dontShowAgainBtn').click(function() {
            // Set a cookie to prevent the myModal modal from showing again
            var expires = new Date();
            expires.setTime(expires.getTime() + 1 * 24 * 60 * 60 * 1000); // 1 day
            document.cookie = "myModalShown=true; expires=" + expires.toUTCString() + "; path=/";
            $('#myModal').modal('hide');
        });
    });
});


