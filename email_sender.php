<?php
use PHPMailer\PHPMailer\PHPMailer;
use PHPMailer\PHPMailer\SMTP;
use PHPMailer\PHPMailer\Exception;

// Include PHPMailer autoload file
require 'phpmailer/PHPMailer.php';
require 'phpmailer/SMTP.php';
require 'phpmailer/Exception.php';

if ($_SERVER["REQUEST_METHOD"] == "POST") {
    $name = $_POST['name'];
    $company = $_POST['company'];
    $phone = $_POST['phone'];
    $to_email = $_POST['email'];
    $message = $_POST['message'];
    $subject = 'INQUIRY - '. $name .' from ' . $company;
    // Read the HTML template
    $html_file_path = 'email-template.html';
    $html_content = file_get_contents($html_file_path);

    // Replace placeholders with actual values
    $html_content = str_replace('{{name}}', $name, $html_content);
    $html_content = str_replace('{{company}}', $company, $html_content);
    $html_content = str_replace('{{phone}}', $phone, $html_content);
    $html_content = str_replace('{{email}}', $to_email, $html_content);
    $html_content = str_replace('{{message}}', $message, $html_content);
    $html_content = str_replace('{{subject}}', $subject, $html_content);

    $mail = new PHPMailer(true);

    try {
        //Server settings
        $mail->isSMTP();
        $mail->Host = 'smtp.office365.com';
        $mail->SMTPAuth = true;
        $mail->Username = 'noreply@industrymoverscorp.com';
        $mail->Password = 'Imc_12345';
        $mail->SMTPSecure = PHPMailer::ENCRYPTION_STARTTLS;
        $mail->Port = 587;
        $mail->CharSet = 'UTF-8';
        //Recipients
        $mail->setFrom('inquiry@industrymoverscorp.com', 'Inquiry');
        //prod
        $mail->addAddress('commercial@industrymoverscorp.com', 'Commercial Sales and Marketing');
        $mail->addCC('eguita@industrymoverscorp.com' , 'Erwin Bob M. Guita');
        $mail->addCC('abismonte@industrymoverscorp.com' , 'Angelika B. Bismonte');
        $mail->addCC('jebianes@industrymoverscorp.com' , 'Jorgie Ehm A. Bianes');
        $mail->addCC('sang@industrymoverscorp.com' , 'Shellane R. Ang');
        $mail->addBCC('oaducal@industrymoverscorp.com' , 'Oscar Aducal Jr.');
        $mail->addBCC('nrreyes@industrymoverscorp.com' , 'Nino Rey Reyes');

        //test
        // $mail->addCC('oaducal@industrymoverscorp.com' , 'Oscar Aducal Jr.');
        // $mail->addBCC('nrreyes@industrymoverscorp.com' , 'Nino');


        //Content
        $mail->isHTML(true);
        $mail->Subject = $subject;
        $mail->Body = $html_content;

        $mail->send();
        echo 'Email sent successfully!';
    } catch (Exception $e) {
        echo 'Failed to send email. Error: ' . $mail->ErrorInfo;
    }
}
?>