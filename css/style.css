/* imc palete */

/* 
#d2eef8 - white
#c78b22 - gold
#cfb53b - light gold
#1a1446 - dark blue
#162619 - lighten green
#122317 - dark green 
*/
html,
body {
  width: 100%;
  height: 100vh;
  margin: 0;
}

* {
  font-family: "League Spartan", sans-serif;
  font-optical-sizing: auto;
  font-style: normal;
  box-sizing: border-box;
}

.custom-font-15 {
  font-size: 15px;
}

/* custom button */

.btn-outline-primary {
  color: #1a1446 !important;
  border-color: #1a1446 !important;
  font-weight: bold;
}

.btn-outline-primary:hover {
  background-color: #c78b22 !important;
  color: #1a1446 !important;
}

.btn-outline-primary.focus,
.btn-outline-primary:focus {
  box-shadow: 0 0 0 0.2rem rgb(210, 238, 248) !important;
}

.imc-text-dark-blue {
  color: #1a1446 !important;
}

.imc-text-light-gold {
  color: #cfb53b !important;
}

.imc-text-gold {
  color: #c78b22 !important;
}

.imc-text-white {
  color: #d2eef8 !important;
}

.imc-text-light-green {
  color: #162619 !important;
}

.imc-text-dark-green {
  color: #122317 !important;
}

.imc-bg-white {
  background-color: white !important;
}

.imc-bg-dark-blue {
  background-color: #1a1446 !important;
}

.imc-bg-gold {
  background-color: #c78b22 !important;
}

p {
  font-family: "Montserrat", sans-serif;
  font-optical-sizing: auto;
  font-style: normal;
}

p>b {
  font-family: "Montserrat", sans-serif;
  font-optical-sizing: auto;
  font-style: normal;
  font-weight: bold;
}

b {
  font-family: "League Spartan", sans-serif;
  font-style: normal;
  font-weight: bold;
}

li.active>a {
  font-weight: bold !important;
  color: #1a1446 !important;
}

.linker {
  cursor: pointer;
}
.linker:hover > .svg-filter {
  filter: brightness(0) saturate(100%) invert(10%) sepia(64%) saturate(1353%) hue-rotate(223deg) brightness(96%) contrast(107%) drop-shadow(0 0 2px #162619);
}
.svg-filter {
  filter: invert(63%) sepia(64%) saturate(539%) hue-rotate(358deg) brightness(81%) contrast(91%);
}


.custom-border-bottom-blue {
  border-bottom: 3.5px solid #1a1446 !important;
}

.custom-border-bottom-blue:after {
  content: '';
  position: absolute;
  border-radius: 0 0px 10px 0;
  bottom: 0;
  left: 0;
  right: 30%;
  background: #cfb53b;
  height: 4px;
}

.custom-border-bottom-blue-r {
  border-bottom: 3.5px solid #1a1446 !important;
}

.custom-border-bottom-blue-r:after {
  content: '';
  position: absolute;
  border-radius: 0 0px 10px 0;
  bottom: 0;
  left: 30%;
  right: 0;
  background: #cfb53b;
  height: 4px;
}

.custom-border-bottom-blue-50 {
  border-bottom: 4px solid #cfb53b;
  position: absolute;
  right: 27%;
  left: 0;
  bottom: 0;
  margin: unset;
}




.service-img {
  position: absolute;
  left: 0;
  top: 0;
  background-image: url('../assets/background-img.svg');
  background-repeat: no-repeat;
  background-position: left;
  background-size: cover;
  width: 100%;
  height: 100%;
}

.service-img2 {
  position: absolute;
  left: 0;
  top: 0;
  background-image: url('../assets/charter/e2.png');
  background-repeat: no-repeat;
  background-position: left;
  background-size: cover;
  width: 100%;
  height: 100%;
}

.imc-divider::after {
  content: '';
  background-image: url('../assets/curvy_eyes.svg');
  background-repeat: no-repeat;
  background-position: left;
  background-size: contain;
  z-index: -1;
  height: 100%;
  top: 0;
  bottom: 0;
  position: absolute;
  border-bottom: 2px solid white !important;
}

.home-top {
  position: relative;
  /* display: flex; */
  height: 100%;
  width: 100%;
}


.video-sm,
.video-md,
.video-lg,
.video-xl {
  position: absolute;
  /* right: 0; */
  top: 0;
  right: 0;
  bottom: 0;
  z-index: -3 !important;
  /* width: 80%!important; */

  /* z-index: 0!important; */
  object-fit: cover;
}


.list-group-item.active {
  z-index: 2;
  color: #fff;
  background-color: #1a1446 !important;
  border-color: #1a1446 !important;
}

.card-img-top-lg,
.card-img-top-sm,
.card-img-top-md,
.card-img-top-xl {
  width: 100%;
  object-fit: cover;
}

/* carousel */
/* bootstrap mod: carousel controls */
.carousel-control-prev-icon,
.carousel-control-next-icon {
  background-image: url("data:image/svg+xml;charset=utf8,%3Csvg xmlns='http://www.w3.org/2000/svg' fill='none' stroke='%2322313F' stroke-miterlimit='10' stroke-width='2' viewBox='0 0 34.589 66.349'%3E%3Cpath d='M34.168.8 1.7 33.268 34.168 65.735'/%3E%3C/svg%3E");
  height: 100px;
}

.carousel-control-next-icon {
  transform: rotate(180deg);
}


.contact-form:before {
  content: "";
  position: absolute;
  width: 26px;
  height: 26px;
  background-color: #1a1446;
  transform: rotate(45deg);
  top: 27px;
  left: -13px;
}

/* medium - display 4  */
@media (min-width: 768px) {

  #gallery .carousel-inner .carousel-item-right.active,
  #gallery .carousel-inner .carousel-item-next {
    transform: translateX(33.33333%);
  }

  #gallery .carousel-inner .carousel-item-left.active,
  #gallery .carousel-inner .carousel-item-prev {
    transform: translateX(-33.33333%);
  }
}

/* large - display 5 */
@media (min-width: 992px) {

  #gallery .carousel-inner .carousel-item-right.active,
  #gallery .carousel-inner .carousel-item-next {
    transform: translateX(20%);
  }

  #gallery .carousel-inner .carousel-item-left.active,
  #gallery .carousel-inner .carousel-item-prev {
    transform: translateX(-20%);
  }
}

#gallery .carousel-inner .carousel-item-right,
#gallery .carousel-inner .carousel-item-left {
  transform: translateX(0);
}


/* gallery slider */
#gallery .carousel-inner .carousel-item.active,
#gallery .carousel-inner .carousel-item-next,
#gallery .carousel-inner .carousel-item-prev {
  display: flex;
  align-items: center;
}

@media (max-width: 768px) {
  #gallery .carousel-inner .carousel-item>div {
    display: none;
  }

  #gallery .carousel-inner .carousel-item>div:first-child {
    display: block;
    text-align: center;
  }
}

#contact_info::after {
  content: '';
  background-image: url('../assets/Homepage.jpg');
  background-size: cover;
  background-repeat: no-repeat;
  position: absolute;
  left: 0;
  right: 0;
  bottom: 0;
  top: 0;
  z-index: -1;
  opacity: 0.4;
}

.background-home::after {
  content: '';
  background-image: url('../assets/background-img.svg');
  background-size: cover;
  background-position: center;
  background-repeat: no-repeat;
  position: absolute;
  left: 0;
  right: 0;
  bottom: 0;
  top: 0;
  z-index: -1;
  opacity: 0.4;
}


.background-privacy::after {
  content: '';
  background-image: url('../assets/privacy.jpg');
  background-size: cover;
  background-position: bottom;
  background-repeat: no-repeat;
  position: absolute;
  left: 0;
  right: 0;
  bottom: 0;
  top: 0;
  z-index: -1;
  opacity: 0.4;
}

.background-news::after {
  content: '';
  background-image: url('../assets/news-background.svg');
  background-size: cover;
  background-position: top;
  background-repeat: no-repeat;
  position: absolute;
  left: 0;
  right: 0;
  bottom: 0;
  top: 0;
  z-index: -1;
  opacity: 0.4;
}

.background-project::after {
  content: '';
  background-image: url('../assets/backgroundss.JPG.png');
  background-size: cover;
  background-position: bottom;
  background-repeat: no-repeat;
  position: absolute;
  left: 0;
  right: 0;
  bottom: 0;
  top: 0;
  z-index: -1;
  opacity: 0.4;
}


.heighten-sense-sm,
.heighten-sense-md,
.heighten-sense-lg

/* bootstrap mod addons */
.w-90 {
  width: 90%;
}

.col-5,
.col-sm-5,
.col-md-5,
.col-lg-5,
.col-xl-5 {
  position: relative;
  width: 100%;
  padding-right: 15px;
  padding-left: 15px;
}

.col-5 {
  flex: 0 0 20%;
  max-width: 20%;
}

.overlay-text-sm>h1,
.overlay-text-md>h1,
.overlay-text-lg>h1,
.overlay-text-xl>h1 {
  font-weight: bold;
}


.text-overlay-lg,
.text-overlay-md,
.text-overlay-sm {
  position: absolute;
  z-index: 2;
  top: 0;
  bottom: 0;
  left: 0;
}

.main-sm,
.main-md,
.main-lg,
.main-xl {
  position: relative;
  width: 100%;
}


.mobiledivider::after {
  content: '';
  background-image: url('../assets/mobilehomepage.png');
  background-repeat: no-repeat;
  background-position: left;
  background-size: cover;
  z-index: -1;
  height: 100%;
  left: 0;
  right: 0;
  position: absolute;
}

#mobility {
  position: absolute;
  /* left: 50%; */
  right: 5%;
}


@media (min-width: 576px) {
  .col-sm-5 {
    flex: 0 0 20%;
    max-width: 20%;
  }

  .card-img-top-sm {
    height: 40vw;
  }

  .heighten-sense-sm {
    height: 100vw;
  }

  .mobile-response {
    background-color: #1a1446 !important;
  }

  .mobile {
    display: none;
  }

  .mobile-content {
    display: block;
  }

  .mobiledivider::after {
    width: 100vw;
  }

  #mobility {
    bottom: 15%;
  }

  #desktop {
    display: none;
  }

}

@media (max-width:767px) {
  .mobile-response {
    background-color: #1a1446 !important;
    height: 50%;
  }

  .mobile-response>* {
    color: white !important;
  }

  .flex-sm-row-reverse {
    flex-direction: row-reverse !important;
  }

  .video-sm {
    width: 100%;
    left: 0;
    height: 56%;
  }

  .overlay-text-sm>h1 {
    font-size: 30px;
  }

  .main-sm {
    height: 81vh;
  }

  .w-sm {
    width: 100%;

  }

  .mobiledivider::after {
    display: block;
  }

  .mobiledivider::after {
    width: 100vw;
    bottom: -4%;
  }

  #mobility {
    bottom: 3%;
  }

  #desktop {
    display: none;
  }
  .orient-sm{
    font-size: 45px!important;
  }
  .orient-sm:nth-child(2){
    margin-top: -20px;
  }
  .orient-sm:nth-child(3){
    margin-top: -20px;
  }
  .w-card-sm {
    width: 98%;
  } 
  .contact-section-sm {
    height: unset;
  }
  .contact-section-sm > div > div  {
    margin-top: 50px;
    margin-bottom: 50px;
  }
  .contact-form:before {
    display: none;
  }
  .logo-sm {
    height: 40px;
  }
  .quote-sm {
    font-size: 25px;
  }
  .head-sm {
    height: 25%;
  }
  .head-sm > div > div > h1 {
    font-size: 30px;
  }

}


@media (min-width: 768px) {
  .col-md-5 {
    flex: 0 0 20%;
    max-width: 20%;
  }

  .card-img-top-md {
    height: 30vw;
  }

  .heighten-sense-md {
    height: 100vw;
  }

  .mobile-response {
    background-color: #1a1446 !important;
  }

  .video-md {
    width: 70% !important;
    right: 0;
    height: 100%;
  }

  .imc-divider::after {
    display: block;
    left: 21% !important;
    width: 70%;
  }

  .overlay-text-md>h1 {
    font-size: 32px;
    width: 70%;
  }

  .overlay-text-md>h1:nth-child(2) {
    margin-top: -20px;
  }

  .overlay-text-md>h1:nth-child(3) {
    margin-top: -20px;
  }

  .overlay-text-md>h1>span {
    font-size: 50px;
  }

  .main-md {
    height: 72vh;
  }

  .w-md {
    width: 40%;
  }
  .w-card-md {
    width: 95%;
  }
  .contact-section-md {
    height: unset;
  }
  .contact-section-sm > div > div  {
    margin-top: 50px;
    margin-bottom: 50px;
  }
  .contact-form:before {
    display: none;
  }

  .logo-md {
    height: 40px;
  }
  
  .quote-md {
    font-size: 30px;
  }
  .head-md {
    height: 30%;
  }
  .head-md > div > div > h1 {
    font-size: 40px;
  }
}

@media (min-width: 992px) {
  .col-lg-5 {
    flex: 0 0 20%;
    max-width: 20%;
  }

  .card-img-top-lg {
    height: 15vw;
  }

  .heighten-sense-lg {
    height: 78%;
  }

  .mobile-response {
    background-color: #1a1446 !important;
  }

  .imc-divider::after {
    display: block;
    left: 19% !important;
    width: 70%;
  }

  .video-lg {
    width: 70%;
    right: 0;
    height: 100%;
  }

  .overlay-text-lg>h1 {
    font-size: 47px;
  }

  .overlay-text-lg>h1:nth-child(2) {
    margin-top: -30px;
  }

  .overlay-text-lg>h1:nth-child(3) {
    margin-top: -30px;
  }

  .overlay-text-lg>h1>span {
    font-size: 70px;
  }

  .main-lg {
    height: 82vh;
  }

  .w-lg {
    width: 50%;
  }
  .w-card-lg {
    width: 90%;
  } 
  .contact-section-lg {
    height: 92vh;
  } 
  .logo-lg {
    height: 50px;
  }
  .quote-lg {
    font-size: 40px;
  }
  .head-lg {
    height: 35%;
  }
  .head-lg > div > div > h1 {
    font-size: 45px;
  }
}

@media (min-width: 1200px) {
  .w-card-xl{
    width: 80%;
  }
  .col-xl-5 {
    flex: 0 0 20%;
    max-width: 20%;
  }

  .card-img-top-xl {
    height: 15vw;
  }

  .mobile-response {
    background-color: unset !important;
  }

  .imc-divider::after {
    /* display: block; */
    left: 9.5% !important;
    width: 90%;
  }

  .video-xl {
    width: 77% !important;
    right: 0;
    height: 100%;
  }

  .overlay-text-xl>h1 {
    font-size: 47px;
    margin-top: -30px;
  }

  .overlay-text-xl>h1:nth-child(2) {
    margin-top: -30px;
  }

  .overlay-text-xl>h1:nth-child(3) {
    margin-top: -30px;
  }

  .overlay-text-xl>h1>span {
    font-size: 70px;
  }

  .main-xl {
    height: 92vh;
  }

  .w-xl {
    width: 50%;
  }

  #desktop {
    margin-top: 40px;
  }
 
  .contact-section-xl {
    height: 92vh;
  }
  .logo-xl {
    height: 50px;
  }
  .quote-xl {
    font-size: 40px;
  }
  .head-xl {
    height: 40%;
  }
  .head-xl > div > div > h1 {
    font-size: 50px;
  }
}


@media screen and (orientation: landscape) {
  .mobiledivider::after {
    display: none;
  }

  .imc-divider::after {
    display: block;
  }

  #mobile {
    display: none;
  }

  #desktop {
    display: block;
  }

  .mobile-content {
    content: '';
    display: block;
  }
}

@media (min-width: 992px) and (orientation: portrait) {
  body::-webkit-scrollbar {
    display: none;
  }
  #mobile {
    display: block;
  }

  #desktop {
    display: none !important;
  }

  .mobiledivider::after {
    width: 100% !important;
    top: 0;
  }

  .video-lg {
    width: 100% !important;
    left: 0;
    right: 0;
    height: 57%;
  }
  #mobility {
    bottom: 13%;
  }

  .main-lg {
    height: 96vh;
  }
  .orient-lg{
    font-size: 120px!important;
  }
  .orient-lg:nth-child(2){
    margin-top: -50px;
  }
  .orient-lg:nth-child(3){
    margin-top: -50px;
  }

  .p-orient-lg {
    font-size: 40px;
    margin-bottom: 50px;
  }
  .btn-size-lg {
    font-size: 32px;
  }
  .contact-section-lg {
    height: 96vh;
  }
  .quote-lg {
    font-size: 50px;
  }
}